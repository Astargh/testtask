import {
  AfterViewInit,
  Directive,
  ElementRef,
  HostListener,
  Input,
  OnChanges,
  Renderer2} from '@angular/core';

@Directive({
  selector: '[appGridDirective]'
})
export class GridDirective implements AfterViewInit, OnChanges {
  @Input() grid: Element;
  @Input() imgLoad: boolean;
  @HostListener('window:resize') onResize() {
    this.setStyle();
  }

  constructor(private el: ElementRef,
              private renderer: Renderer2) {
  }

  ngOnChanges() {
    if (this.imgLoad) this.setStyle();
  }

  ngAfterViewInit() {
    this.setStyle();
  }

  setStyle() {
    const styles = getComputedStyle(this.grid);
    const rowHeight = Number(styles.gridAutoRows.replace('px', ''));
    const rowGap = Number(styles.gridRowGap.replace('px', ''));
    const rowSpan = Math.ceil((this.el.nativeElement.childNodes[1].getBoundingClientRect().height + rowGap) / ( rowHeight + rowGap));
    this.renderer.setStyle(this.el.nativeElement, 'grid-row-end', `span ${rowSpan}`);
  }
}
