import {ItemInterface} from './item.interface';

export class Item implements ItemInterface{
  public title: string;
  public image: string;
  public desc: string;
  public imgLoad = false;

  constructor(data) {
    if (data) {
      this.title = data.title;
      this.image = data.image;
      this.desc = data.desc;
    }
  }
}
