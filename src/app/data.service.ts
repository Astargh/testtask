import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Item} from './item';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) {
  }

  public getTestData() {
    return this.http.get('assets/data/test.json')
      .pipe(map((res: any) => res.items))
      .pipe(map(res => {
          return res.filter(item => item.hasOwnProperty('content'))
            .map(item => {
              return new Item({
                title: item.content.title,
                image: item.content.image.url,
                desc: item.content.desc,
                imgLoad: false
              });
            });
          }
        )
      );
  }
}
