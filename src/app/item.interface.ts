export interface ItemInterface {
  title: string;
  image: string;
  desc: string;
  imgLoad: boolean;
}
