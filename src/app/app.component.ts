import {Component, ElementRef, ViewChild} from '@angular/core';
import {DataService} from './data.service';
import {Item} from './item';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public items: Array<Item>;

  @ViewChild('grid') grid: ElementRef;

  constructor(private httpData: DataService) {
    this.getData();
  }

  getData() {
    this.httpData.getTestData()
      .subscribe(
        (res: Array<Item>) => {
          this.items = res;
        });
  }
}
